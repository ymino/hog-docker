FROM cern/cc7-base:latest

RUN yum update -y

RUN yum install -y http://opensource.wandisco.com/centos/7/git/x86_64/wandisco-git-release-7-2.noarch.rpm

RUN yum update -y && \
    yum install -y man && \
    yum -y install uuid && \
    yum -y install uuid-devel && \
    yum -y install libuuid-devel && \
    yum -y install git && \
    yum -y install file && \
    yum -y install which && \
    yum -y install cmake && \
    yum -y install gcc && \
    yum -y install python && \
    yum -y install make && \
    yum -y install freetype && \
    yum -y install doxygen && \
    yum -y install krb5-workstation rsync openssh-clients xrootd-client mkdocs && \
    yum -y install epel-release && \
    yum -y install centos-release-scl shellcheck && \
    yum -y install rh-python36 unzip && \
    yum -y install texlive-epstopdf
    

RUN yum -y install perl-YAML cvsps perl-CGI perl-DBI subversion-perl cvs tk perl subversion perl-Authen-SASL perl-Digest-MD5 perl-Net-SMTP-SSL
RUN yum -y --disablerepo=base,updates  update git

# COPY scripts /scripts/
RUN mkdir -p /usr/local/doxverilog/bin
COPY bin/doxygen /usr/local/doxverilog/bin/doxverilog
ENV PATH $PATH:/usr/local/doxverilog/bin
RUN which doxverilog

# RUN useradd -c 'clang-format-bot' -m -d /home/clang -s /bin/bash clang
# USER clang
# ENV HOME /home/clang

CMD bash
